# O2MC Dimml SLOC README #

Adversitement's platform of choice for online data collection, processing, and distribution is [O2MC's Dimml Platform](http://www.dimml.io).
The Dimml platform is also used in Adversitement's data quality solution called [Qmon](http://www.qmon.io) to collect data with high-availibiliy and performance.


In order to deploy [Qmon](http://www.qmon.io), [DataStreams](http://www.datastreams.io), or a custom Dimml solution on a website, a single line of code (SLOC) needs to be added to all pages of a site.


```
(function(){ var a=document.getElementsByTagName('script')[0], b=document.createElement('script'); b.async=!0;b.src='//cdn.dimml.io/dimml.js'; a.parentNode.insertBefore(b,a); })();
```


Typically the platform is loaded asynchronously at the bottom of a page.


See [example.html](./example.html) for an example page.